FROM igwn/base:el9

LABEL name="Markdown Lint"
LABEL maintainer="Adam Mercer <adam.mercer@ligo.org>"
LABEL support="Best Effort"

# install available updates
RUN dnf -y update && dnf clean all

# install nodejs
RUN update-crypto-policies --set DEFAULT:SHA1 && \
    rpm --import https://rpm.nodesource.com/gpgkey/nodesource.gpg.key && \
    dnf -y install https://rpm.nodesource.com/pub_20.x/nodistro/repo/nodesource-release-nodistro-1.noarch.rpm && \
    dnf -y install nodejs --setopt=nodesource-nodejs.module_hotfixes=1 && \
    dnf clean all && \
    update-crypto-policies --set DEFAULT

# install markdown lint
RUN npm install -g markdownlint-cli &&\
    npm cache clean --force
